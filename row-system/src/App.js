import React from 'react';
import Routes from './routes'

import {Provider} from 'react-redux';
import store from './redux/store'

function App() {
  return (
    <Provider store={store}>
    <div >
      <Routes />
    </div>
    </Provider>
  );
}

export default App;
