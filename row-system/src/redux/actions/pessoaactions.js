import pessoaservices from "../../services/pessoaservices";

export const PESSOA_ACTIONS = {
    LISTAR : "PESSOA_LISTAR",
    BUSCAR : "PESSOA_BUSCAR",
    SALVAR : "PESSOA_SALVAR",
    ALTERAR : "PESSOA_ALTERAR",
    EXCLUIR : "PESSOA_EXCLUIR"
}

export function listarPessoa(){

    return function(dispatch){
        return pessoaservices.listar()
        .then( response=> {
            dispatch({
                type: PESSOA_ACTIONS.LISTAR,
                payload: response
            })
        })
        .catch(error =>{
            console.log(error);
        })
    }
}

export function excluirPessoa(id){

    return function(dispatch){
        return pessoaservices
        .delete(id)
        .then(response => {
            dispatch({
                type: PESSOA_ACTIONS.EXCLUIR,
                payload: id // passa o id da pessoa excluida
            });
        } )
        .catch(error => {
            console.log(error);
        })
    }
}