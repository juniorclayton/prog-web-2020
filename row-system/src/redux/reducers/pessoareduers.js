import {PESSOA_ACTIONS} from "../actions/pessoaactions";

const pessoaeState = {
    pessoaLista: [],
    pessoaItem: {}
}

export default function pessoaReducers( state = pessoaeState, dispatch){

    switch(dispatch.action){
        
        case PESSOA_ACTIONS.LISTAR:
            return {
                ...state,
                pessoaLista: dispatch.content
            }
        
        default:
            return state;
    }
}