import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import bg from '../../assets/bg.png'
import logo from '../../assets/logo.png'
import {Link} from 'react-router-dom'

const Home = () => {
    const [descricao, setDescricao] = useState('Para empresas da indústria têxtil, que precisam de uma melhor comunicação com as faccionistas e necessitam agilizar o processo de produção de peças, nosso software automatizará o processo de comunicação entre a empresa e as costureiras através de alguns formulários, que devem conter o material a ser entregue à costureira, peça ou modelo que ela está trabalhando, últimas decisões estabelecidas, prazo de entrega, também deve conter o material devolvido pela costureira e informações sobre as peças defeituosas. O diferencial de nosso sistema será a sua facilidade de uso e sua simplicidade.');  
    
    return (
        <div className='container'>
            <header>
                <div className='logo'>
                <img src={logo} className='imagemlogo'></img>
                </div>
                <div className='menu'>
                    <button><Link to = '/'>Home</Link></button>
                    <button><Link to = '/cadastro'>Cadastrar</Link></button>
                    <button><Link to = '/listar'>Listar cadastros</Link></button>
                </div>
                
            </header>

            <div className='conteudo'>
                <img src={bg} className='imagembg'></img>
                <h1>Sistema Row System</h1>
                <p className='descricao'>{descricao}</p>
            </div>

            <div className='rodape'>
                <h1>Contato</h1>
                <p>rowsystemerp@rowsystem.com</p>
                <p>(62) 94002-8922</p>
                <p>Viela VP-02 DA QD.06 MD.08 - DAIA</p>
                <p>Anápolis / GO - CEP 75.132-055</p>
            </div>
        </div>
    )
}

export default Home;