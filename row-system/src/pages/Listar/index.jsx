import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import bg from '../../assets/bg.png';
import logo from '../../assets/logo.png';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";

import {listarPessoa} from '../../redux/actions/pessoaactions';
import {pessoaListaSelector} from '../../redux/selectors/pessoaslectors';


const Listar = () => {

    class PessoaLista extends React.Component{
        constructor(props, context){
            super(props, context);
            
                this.props.listarPessoa()
                .then()
                .catch();
            

        const [descricao, setDescricao] = useState('Para empresas da indústria têxtil, que precisam de uma melhor comunicação com as faccionistas e necessitam agilizar o processo de produção de peças, nosso software automatizará o processo de comunicação entre a empresa e as costureiras através de alguns formulários, que devem conter o material a ser entregue à costureira, peça ou modelo que ela está trabalhando, últimas decisões estabelecidas, prazo de entrega, também deve conter o material devolvido pela costureira e informações sobre as peças defeituosas. O diferencial de nosso sistema será a sua facilidade de uso e sua simplicidade.');  
                
        return (

            <div className='container'>
                <header>
                    <div className='logo'>
                    <img src={logo} className='imagemlogo'></img>
                    </div>
                    <div className='menu'>
                        <button><Link to = '/'>Home</Link></button>
                        <button><Link to = '/cadastro'>Cadastrar</Link></button>
                        <button><Link to = '/listar'>Listar cadastros</Link></button>
                    </div>
                    
                </header>

                <div className='listar'>
                    
                    <h1>Lista de pessoas</h1>

                    <table className='table'>
                        <thead>
                            <tr>
                                <th scope='col'>#ID</th>
                                <th scope='col'>Nome</th>
                            </tr>
                        </thead>
                        <tbody>{

                            this.props.pessoaLista.map( pessoa => 
                                <tr key={pessoa.id}>
                                    <td>{pessoa.id}</td>
                                    <td>{pessoa.nome}</td>
                                </tr>
                            )
                        }

                        </tbody>
                    </table>

                </div>

                <div className='rodape'>
                    <h1>Contato</h1>
                    <p>rowsystemerp@rowsystem.com</p>
                    <p>(62) 94002-8922</p>
                    <p>Viela VP-02 DA QD.06 MD.08 - DAIA</p>
                    <p>Anápolis / GO - CEP 75.132-055</p>
                </div>
            </div>
            )
        }
    }
}

export default connect(pessoaListaSelector, {listarPessoa})(Listar);