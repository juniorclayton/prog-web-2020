import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom'

import Home from './pages/Home'
import Cadastro from './pages/Cadastro'
import Listar from './pages/Listar'


const Routes = () => {
    return (
        <BrowserRouter>
            <Route component={Home} path="/" exact />
            <Route component={Cadastro} path="/cadastro" exact />
            <Route component={Listar} path="/listar" exact />
        </BrowserRouter>
    )
}

export default Routes;