import axios from 'axios';

class PessoaService {

    constructor(){
        this.conection = axios.create({baseURL: 'http://localhost:8080'});
    }

    listar(){
        return this.conection.get('/pessoa');
    }

    buscar(id){
        return this.conection.get('/pessoa/+id');
    }

    salvar(pessoa){

        if(pessoa.id){
            return this.conection.put('/pessoa', pessoa);            
        }
        return this.conection.post('/pessoa', pessoa);
    }
    excuir(id){
        return this.connection.delete('/carro'+id);
    }
}

export default new PessoaService();